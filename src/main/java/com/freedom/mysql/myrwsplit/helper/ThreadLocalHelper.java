package com.freedom.mysql.myrwsplit.helper;

import org.apache.ibatis.mapping.SqlCommandType;

public class ThreadLocalHelper {
	public static void reset() {
		// reset all,reset everything
		SqlCommandTypeThreadLocal.set(SqlCommandType.UNKNOWN);
		AutoCommitThreadLocal.set(true);
		BoundSqlThreadLocal.set(null);
	}

	public static ThreadLocal<SqlCommandType> SqlCommandTypeThreadLocal = new ThreadLocal<SqlCommandType>() {

		public SqlCommandType initialValue() {
			return SqlCommandType.UNKNOWN;
		}
	};

	public static ThreadLocal<Boolean> AutoCommitThreadLocal = new ThreadLocal<Boolean>() {

		public Boolean initialValue() {// 默认自动提交
			return true;
		}
	};

	public static ThreadLocal<String> BoundSqlThreadLocal = new ThreadLocal<String>() {
		public String initialValue() {
			return null;
		}
	};
}
